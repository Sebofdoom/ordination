package ordination;

import java.time.*;
import java.util.ArrayList;

public class DagligFast extends Ordination{
    private ArrayList<Dosis> doser = new ArrayList<Dosis>();

    public DagligFast(LocalDate start, LocalDate slut, Laegemiddel laegemiddel){
        super(start, slut, laegemiddel);
    }

    public ArrayList<Dosis> getDoser() {
        return new ArrayList<Dosis>(this.doser);
    }

    public void addDosis(Dosis dosis) {
        this.doser.add(dosis);

    }

    public void removeDosis(Dosis dosis){
        this.doser.remove(dosis);

    }

    public Dosis createDosis(LocalTime tid, double antal) {
        Dosis temp = new Dosis(tid, antal);
        addDosis(temp);
        return temp;
    }

    @Override
    public double samletDosis() {
        return 0;
    }

    @Override
    public double doegnDosis() {
        return 0;
    }

    @Override
    public String getType() {
        return null;
    }
}
